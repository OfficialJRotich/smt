#! /bin/bash
clear
echo " ______        ___    _   _ ___ _     ___ _   _ _   ___  __"
echo "/ ___\ \      / / \  | | | |_ _| |   |_ _| \ | | | | \ \/ /"
echo "\__ _ \\ \ /\ / / _ \ | |_| || || |    | ||  \| | | | |\  / "
echo " ___) |\ V  V / ___ \|  _  || || |___ | || |\  | |_| |/  \ "
echo "|____/  \_/\_/_/   \_\_| |_|___|_____|___|_| \_|\___//_/\_\\"
echo "                                                           "

##Install Swahili Locale
echo "I am now setting up locale to Swahili"
echo "We are currently translating MATE Desktop Environment"
echo "If you want to have your applications in Swahili, Kindly 
install the MATE Desktop Environment."
echo "Your current locale is set to"

localectl list-locales

echo "Are you sure that you want to change?"
echo "1. Yes, sure"
echo "2. No, take me back" 

read choice

if [ $choice -eq 1 ] ; then
echo "Changing locale..."
sleep 2
sudo update-locale LANG=sw_KE.UTF-8
sleep 2
LANG=sw_KE.UTF-8
sleep 2
LC_MESSAGES=POSIX
echo "Here are the results"
echo ""
sleep 2
sudo cat /etc/default/locale
sleep 5
echo "Taking you back to main menu. Please wait..."
sleep 5
echo ""
./install.sh
else
if [ $choice -eq 2 ] ; then
echo "Taking you back..."
./install.sh
else
$choice=6
fi
fi
