#! /bin/bash
clear
echo " ______        ___    _   _ ___ _     ___ _   _ _   ___  __"
echo "/ ___\ \      / / \  | | | |_ _| |   |_ _| \ | | | | \ \/ /"
echo "\__ _ \\ \ /\ / / _ \ | |_| || || |    | ||  \| | | | |\  / "
echo " ___) |\ V  V / ___ \|  _  || || |___ | || |\  | |_| |/  \ "
echo "|____/  \_/\_/_/   \_\_| |_|___|_____|___|_| \_|\___//_/\_\\"
echo "                                                           "

##
echo "Migration has started succesfully, Please wait..."
echo "Removing /etc/lsb-release and /etc/os-release"
echo "Confirming administrative priviledges..."

sudo rm -r /etc/os-release && sudo rm -r /etc/lsb-release

##Copy etc to /etc
sudo cp -r etc/* /etc/
echo "Migration has finished succesfully"
echo "We are collecting information about changes made on you Operating System"
echo "Kindly share the zipped file with us for analysis"
echo "We have only collected the name of your distribution to see if the program works"
echo "as expected"
sleep 5
./install.sh
