#! /bin/bash
clear
echo " ______        ___    _   _ ___ _     ___ _   _ _   ___  __"
echo "/ ___\ \      / / \  | | | |_ _| |   |_ _| \ | | | | \ \/ /"
echo "\__ _ \\ \ /\ / / _ \ | |_| || || |    | ||  \| | | | |\  / "
echo " ___) |\ V  V / ___ \|  _  || || |___ | || |\  | |_| |/  \ "
echo "|____/  \_/\_/_/   \_\_| |_|___|_____|___|_| \_|\___//_/\_\\"
echo "                                                           "

echo "Changing look and feel"
echo "Please wait..."

##Remove all other backgrounds and icons
##sudo rm -r /usr/share/backgrounds/ && sudo rm -r /usr/share/icons/

##blank directories
##sudo mkdir /usr/share/backgrounds/ && sudo mkdir /usr/share/icons/

##Copy icons and backgrounds
sudo cp -r usr/share/icons/* /usr/share/icons && sudo cp -r usr/share/backgrounds/* /usr/share/backgrounds/

echo "Swahilinux backgrounds and icons have been succesfully added to your system"
echo "Navigate to appearance settings to change to swahilinux Look and Feel"
