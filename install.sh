#! /bin/bash
clear
echo " ______        ___    _   _ ___ _     ___ _   _ _   ___  __"
echo "/ ___\ \      / / \  | | | |_ _| |   |_ _| \ | | | | \ \/ /"
echo "\__ _ \\ \ /\ / / _ \ | |_| || || |    | ||  \| | | | |\  / "
echo " ___) |\ V  V / ___ \|  _  || || |___ | || |\  | |_| |/  \ "
echo "|____/  \_/\_/_/   \_\_| |_|___|_____|___|_| \_|\___//_/\_\\"
echo "                                                           "
                                                           
#Collect Username
user=$(whoami)

#Welcome User
echo "Greetings $user, welcome to Swahilinux 1.0 Moran"
echo "1. Change OS to Swahilinux Moran"
echo "2. Change my OS appearance to depict Swahilinux"
echo "3. Change locale to Swahili"
echo "4. All of the above"
echo "5. Exit Application"
echo ""
echo "What would you like to do?" 

read choice

if [ $choice -eq 1 ] ; then
## Make Migration script executable
sudo chmod +x migrate.sh

## Run Migration
sudo ./migrate.sh
else
if [ $choice -eq 2 ] ; then
## Make appearance script executable
sudo chmod +x appearance.sh

## Run Appearance script
sudo ./appearance.sh

else
if [ $choice -eq 3 ] ; then
## Install Swahili Locale
sudo ./locale.sh
else
if [ $choice -eq 4 ] ; then
##Do all at once
## Make Migration script executable
sudo chmod +x migrate.sh

## Run Migration
sudo ./migrate.sh

## Make appearance script executable
sudo chmod +x appearance.sh

## Run Appearance script
sudo ./appearance.sh
## Install Swahili Locale
sudo ./locale.sh
sleep 2
echo "Thank you for becoming one of us!"
else
if [ $choice -eq 5 ] ; then
#Print Goodbye Message
echo "Thank you for Using SMT"
sleep 2
echo "Exiting now..."
sleep 2
echo `exit`
else
echo "You did not choose correctly"
fi
fi
fi
fi
fi
